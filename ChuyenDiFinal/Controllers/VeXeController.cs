﻿using ChuyenDiFinal.Models.ViewModel;
using ChuyenDiFinal.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ChuyenDiFinal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VeXeController : ControllerBase
    {
        private readonly VeXeService _vexeservice;
        public VeXeController(VeXeService vexeservice)
        {
            _vexeservice = vexeservice;
        }
        [HttpPost("mua-ve-xe")]
        public IActionResult MuaVeXe([FromBody] MuaVeXe vexe)
        {
            return Ok(_vexeservice.MuaVeXe(vexe));
        }
        [HttpPost("doi-soat-ve-xe")]
        public IActionResult DoiSoatVeXe([FromBody] DoiSoatVeXe doisoat)
        {
            return Ok(_vexeservice.DoiSoatVeXe(doisoat));
        }
        [HttpGet("thong-ke-so-luong-ve-ban-theo-tung-chuyen-di")]
        public IActionResult ThongKeBanVeXe()
        {
            return Ok(_vexeservice.ThongKeBanVeXe());
        }
        [HttpGet("thong-ke-hanh-khach-mua-ve-theo-chuyen-di")]
        public IActionResult ThongKeHanhKhach()
        {
            return Ok(_vexeservice.DanhSachHanhKhach());
        }
        [HttpGet("thong-ke-ve-ban-theo-hanh-khach")]
        public IActionResult ThongKeVeBan()
        {
            return Ok(_vexeservice.ThongKeVeBan());
        }
    }
}
