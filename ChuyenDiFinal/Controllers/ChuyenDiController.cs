﻿using ChuyenDiFinal.Models.Database;
using ChuyenDiFinal.Models.ViewModel;
using ChuyenDiFinal.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ChuyenDiFinal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChuyenDiController : ControllerBase
    {
        private readonly ChuyenDiService _chuyenDiService;
        public ChuyenDiController(ChuyenDiService chuyenDiService)
        {

            _chuyenDiService = chuyenDiService;
        }
        [HttpPost("them-chuyen-di")]
        public IActionResult ThemChuyenDi([FromBody] ThemChuyenDiInput input)
        {
            return Ok(_chuyenDiService.ThemChuyenDi(input));
        }
        [HttpPut("chuyen-trang-thai")]
        public IActionResult ChuyenTrangThai(Guid id)
        {
            return Ok(_chuyenDiService.ChuyenTrangThai(id));
        }
        [HttpPut("huy-chuyen-di")]
        public IActionResult HuyChuyendi([FromBody] HuyChuyenDi cd, Guid id)
        {
            return Ok(_chuyenDiService.HuyChuyenDi(id, cd));
        }
        [HttpGet("danh-sach-chuyen-di-hoan-thanh-tu-ben-xe-thai-nguyen")]
        public IActionResult DSChuyendiChonLoc()
        {
            return Ok(_chuyenDiService.DSChuyendiHoanThanh());
        }
        [HttpGet("DS_Chuyen_Di_Huy")]
        public IActionResult DSChuyenHuy()
        {
            return Ok(_chuyenDiService.DSChuyenHuy());
        }
    }
}
