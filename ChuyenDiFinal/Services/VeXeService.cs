﻿using ChuyenDiFinal.Models.Database;
using ChuyenDiFinal.Models.ViewModel;
using ChuyenDiFinal.Services.HeThong;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace ChuyenDiFinal.Services
{
    public class VeXeService
    {
        private readonly ChuyenDiContext _context;
        private readonly ILogger<VeXeService> _logger;

        private Guid TRANG_THAI_DANG_THUC_HIEN = new Guid("402418f5-687b-4635-9829-a5ccab33e91d");
        public VeXeService(ChuyenDiContext context, ILogger<VeXeService> logger)
        {
            _context = context;
            _logger = logger;
        }        
        //Mua ve xe
        public bool MuaVeXe(MuaVeXe vexe)
        {
            try
            {
                _logger.LogInformation($"Thực hiện MuaVeCD - param={vexe.Object2Json()}");
                var hanhKhach = new Hanhkhach
                {
                    IdHanhKhach = Guid.NewGuid(),
                    HoTen = vexe.HoTen,
                    SoDienThoai = vexe.SoDienThoai,
                    Email = vexe.Email
                };
                var muaVe = new Muave
                {
                    IdHanhKhachNavigation = hanhKhach,
                    IdMuaVe = Guid.NewGuid(),
                    IdChuyenDi = vexe.IdChuyenDi,
                    IdHanhKhach = hanhKhach.IdHanhKhach,
                    ThoiGianMuaVe = DateTime.Now
                };
                _logger.LogInformation("Kiểm tra");
                var chuyenDi = _context.Chuyendis.FirstOrDefault(x => x.IdChuyenDi == vexe.IdChuyenDi);
                var sdtHanhKhach = _context.Hanhkhaches.FirstOrDefault(y => y.SoDienThoai == vexe.SoDienThoai);
                if (chuyenDi.IdTrangThai == TRANG_THAI_DANG_THUC_HIEN && sdtHanhKhach == null)
                {
                    _context.Muaves.Add(muaVe);
                    _context.Hanhkhaches.Add(hanhKhach);
                    _logger.LogInformation("Lưu Dữ liệu vào Database");
                    _context.SaveChanges();
                    _logger.LogInformation("Kết thúc MuaVeCD");
                    return true;
                }
                else
                {
                    if (chuyenDi.IdTrangThai == TRANG_THAI_DANG_THUC_HIEN && sdtHanhKhach != null)
                    {
                        var updHanhKhach = _context.Hanhkhaches.FirstOrDefault(z => z.IdHanhKhach == sdtHanhKhach.IdHanhKhach);
                        updHanhKhach.HoTen = vexe.HoTen;
                        updHanhKhach.SoDienThoai = vexe.SoDienThoai;
                        updHanhKhach.Email = vexe.Email;
                        var updMuaVe = new Muave
                        {
                            IdHanhKhachNavigation = updHanhKhach,
                            IdMuaVe = Guid.NewGuid(),
                            IdChuyenDi = vexe.IdChuyenDi,
                            IdHanhKhach = hanhKhach.IdHanhKhach,
                            ThoiGianMuaVe = DateTime.Now
                        };
                        _context.Muaves.Add(updMuaVe);
                        _logger.LogInformation("Lưu Dữ liệu vào Database");
                        _context.SaveChanges();
                        _logger.LogInformation("Kết thúc MuaVeCD");
                        return true;
                    }
                    else
                    {
                        _logger.LogInformation("Trạng thái không hợp lệ");
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error");
                return false;
            }
        }
        //doi soat ve xe
        public bool DoiSoatVeXe(DoiSoatVeXe doisoat)
        {
            try
            {
                _logger.LogInformation($"Thực hiện DoiSoat - param={doisoat.Object2Json()}");
                var kiemTraIdVe = _context.Muaves.FirstOrDefault(x => x.IdMuaVe == doisoat.IdMuaVe && x.IdChuyenDi == doisoat.IdChuyenDi);
                if (kiemTraIdVe != null)
                {
                    _logger.LogInformation("Thành công");
                    var doiSoat = new Doisoat
                    {
                        IdDoiSoat = Guid.NewGuid(),
                        IdMuaVe = doisoat.IdMuaVe,
                        ThoiGianDoiSoat = DateTime.Now
                    };
                    _context.Doisoats.Add(doiSoat);
                    _context.SaveChanges();
                    return true;
                }
                else
                {
                    _logger.LogInformation("Vé không thuộc chuyến đi");
                    return false;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error");
                return false;
            }
        }
        //thong so luong ke ve xe
        public List<ThongKeVeXe> ThongKeBanVeXe()
        {
            try
            {
                _logger.LogInformation("Thực Hiện thống kê số lượng vé bán theo từng chuyến đi");
                var thongke = from chuyendi in _context.Chuyendis
                              join phuongtien in _context.ChuyendiThongtinphuongtiens
                              on chuyendi.IdChuyenDi equals phuongtien.IdChuyenDi
                              join tuyenduong in _context.Tuyenduongs on chuyendi.IdTuyenDuong equals tuyenduong.IdTuyenDuong
                              join bendi in _context.Benxes on tuyenduong.IdBenXuatPhat equals bendi.IdBenXe
                              join benden in _context.Benxes on tuyenduong.IdBenDen equals benden.IdBenXe
                              join vexe in _context.Muaves on chuyendi.IdChuyenDi equals vexe.IdChuyenDi
                              group new { chuyendi, phuongtien, tuyenduong, bendi, benden, vexe }
                              by new
                              {
                                  chuyendi.MaChuyenDi,
                                  TenBenDi = bendi.TenBenXe,
                                  TenBenDen = benden.TenBenXe,
                                  chuyendi.ThoiGianKhoiHanh,
                                  chuyendi.ThoiGianKetThuc,
                                  phuongtien.BienKiemSoat,
                                  chuyendi.GiaVe,
                                  vexe.IdChuyenDi
                              } into groupChuyenDi
                              select new ThongKeVeXe
                              {
                                  MaChuyenDi = groupChuyenDi.Key.MaChuyenDi,
                                  BenDi = groupChuyenDi.Key.TenBenDi,
                                  BenDen = groupChuyenDi.Key.TenBenDen,
                                  ThoiGianKhoiHanh = groupChuyenDi.Key.ThoiGianKhoiHanh,
                                  ThoiGianKetThuc = groupChuyenDi.Key.ThoiGianKetThuc,
                                  BienKiemSoat = groupChuyenDi.Key.BienKiemSoat,
                                  SoLuongVeBan = groupChuyenDi.Count(x => x.vexe.IdChuyenDi == x.chuyendi.IdChuyenDi),
                                  TongTien = groupChuyenDi.Count(x => x.vexe.IdChuyenDi == x.chuyendi.IdChuyenDi) * groupChuyenDi.Key.GiaVe
                              };
                if (thongke.ToList() == null || thongke.ToList().Count == 0)
                {
                    _logger.LogInformation("Không có chuyến đi nào");
                }
                else
                {
                    _logger.LogInformation("Liệt kê thành công");
                }
                return thongke.ToList();
            }catch(Exception e)
            {
                _logger.LogError(e, "Error");
                return new List<ThongKeVeXe>();
            }
            
        }
        public List<ThongKeHanhKhach> DanhSachHanhKhach()
        {
            try
            {
                _logger.LogInformation("Thực hiện thống kê hành khách theo từng chuyến đi");
                var thongkehanhkhach = from chuyendi in _context.Chuyendis
                                       join phuongtien in _context.ChuyendiThongtinphuongtiens
                                       on chuyendi.IdChuyenDi equals phuongtien.IdChuyenDi
                                       join tuyenduong in _context.Tuyenduongs on chuyendi.IdTuyenDuong equals tuyenduong.IdTuyenDuong
                                       join bendi in _context.Benxes on tuyenduong.IdBenXuatPhat equals bendi.IdBenXe
                                       join benden in _context.Benxes on tuyenduong.IdBenDen equals benden.IdBenXe
                                       join vexe in _context.Muaves on chuyendi.IdChuyenDi equals vexe.IdChuyenDi
                                       join hanhkhach in _context.Hanhkhaches on vexe.IdHanhKhach equals hanhkhach.IdHanhKhach
                                       group new { chuyendi, phuongtien, tuyenduong, bendi, benden, vexe, hanhkhach }
                                       by new
                                       {
                                           chuyendi.MaChuyenDi,
                                           chuyendi.ThoiGianKhoiHanh,
                                           chuyendi.ThoiGianKetThuc,
                                           BienKiemSoat = phuongtien.BienKiemSoat,
                                           TenBenDi = bendi.TenBenXe,
                                           TenBenDen = benden.TenBenXe
                                       } into groupHanhKhach
                                       select new ThongKeHanhKhach
                                       {
                                           MaChuyenDi = groupHanhKhach.Key.MaChuyenDi,
                                           BenDi = groupHanhKhach.Key.TenBenDi,
                                           BenDen = groupHanhKhach.Key.TenBenDen,
                                           ThoiGianKhoiHanh = groupHanhKhach.Key.ThoiGianKhoiHanh,
                                           ThoiGianKetThuc = groupHanhKhach.Key.ThoiGianKetThuc,
                                           BienKiemSoat = groupHanhKhach.Key.BienKiemSoat,
                                           ThongTinKhachHang = groupHanhKhach.Select(x => new KhachHang
                                           {
                                               SoDienThoai = x.hanhkhach.SoDienThoai,
                                               HoTen = x.hanhkhach.HoTen,
                                               Email = x.hanhkhach.Email
                                           }).ToList()
                                       };
                if (thongkehanhkhach.ToList() == null || thongkehanhkhach.ToList().Count == 0)
                {
                    _logger.LogInformation("Không có chuyến đi nào");
                }
                else
                {
                    _logger.LogInformation("Liệt kê thành công");
                }
                return thongkehanhkhach.ToList();
            }catch(Exception e)
            {
                _logger.LogError(e, "Error");
                return new List<ThongKeHanhKhach>();
            }
            
        }
        public List<VeBanHanhKhach> ThongKeVeBan()
        {
            try
            {
                _logger.LogInformation("Thống kê vé bán theo từng hành khách");
                var vehanhkhach = from hanhkhach in _context.Hanhkhaches
                                  join muave in _context.Muaves on hanhkhach.IdHanhKhach equals muave.IdHanhKhach
                                  join chuyendi in _context.Chuyendis on muave.IdChuyenDi equals chuyendi.IdChuyenDi
                                  join phuongtien in _context.ChuyendiThongtinphuongtiens on chuyendi.IdChuyenDi equals phuongtien.IdChuyenDi
                                  join tuyenduong in _context.Tuyenduongs on chuyendi.IdTuyenDuong equals tuyenduong.IdTuyenDuong
                                  join bendi in _context.Benxes on tuyenduong.IdBenXuatPhat equals bendi.IdBenXe
                                  join benden in _context.Benxes on tuyenduong.IdBenDen equals benden.IdBenXe
                                  group new { hanhkhach, muave, chuyendi, phuongtien, bendi, benden }
                                  by new
                                  {
                                      hanhkhach.SoDienThoai,
                                      hanhkhach.HoTen,
                                      hanhkhach.Email,

                                  } into groupVeKhach
                                  select new VeBanHanhKhach
                                  {
                                      SoDienThoai = groupVeKhach.Key.SoDienThoai,
                                      HoTen = groupVeKhach.Key.HoTen,
                                      Email = groupVeKhach.Key.Email,
                                      DanhSachDaMua = groupVeKhach.Select(x => new DanhSachChuyenDiDaMua
                                      {
                                          MaChuyenDi = x.chuyendi.MaChuyenDi,
                                          BienKiemSoat = x.phuongtien.BienKiemSoat,
                                          BenDi = x.bendi.TenBenXe,
                                          BenDen = x.benden.TenBenXe
                                      }).ToList()
                                  };
                if (vehanhkhach.ToList() == null || vehanhkhach.ToList().Count == 0)
                {
                    _logger.LogInformation("Không có Vé");
                }
                else
                {
                    _logger.LogInformation("Liệt kê thành công");
                }
                return vehanhkhach.ToList();
            }catch(Exception e)
            {
                _logger.LogError(e, "Error");
                return new List<VeBanHanhKhach>();
            }
        }
    }
}
