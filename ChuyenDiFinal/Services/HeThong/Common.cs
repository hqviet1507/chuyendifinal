﻿using Newtonsoft.Json;

namespace ChuyenDiFinal.Services.HeThong
{
    public static class Common
    {
        public static string Object2Json(this object a)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(a, new JsonSerializerSettings
            {
                MaxDepth = 1
            });
        }
    }
}
