﻿using ChuyenDiFinal.Models.Database;
using ChuyenDiFinal.Models.ViewModel;

namespace ChuyenDiFinal.Services
{
    public class ChuyenDiService
    {
        private readonly ChuyenDiContext _context;
        private readonly ILogger<ChuyenDiService> _logger;

        private Guid TRANG_THAI_HUY = new Guid("6b616ec2-f68e-4d57-9aa9-16dca86f782c");
        private Guid TRANG_THAI_DANG_THUC_HIEN = new Guid("402418f5-687b-4635-9829-a5ccab33e91d");
        private Guid TRANG_THAI_DA_HOAN_THANH = new Guid("c8b27996-a001-4495-9320-4a6dbbc96d76");
        private Guid ID_BEN_XE_THAI_NGUYEN = new Guid("ee3de232-2fc4-469a-9110-6baf210f988e");
        public ChuyenDiService(ChuyenDiContext context, ILogger<ChuyenDiService> logger)
        {
            _context = context;
            _logger = logger;
        }
        // them chuyen di
        public bool ThemChuyenDi(ThemChuyenDiInput input)
        {
            try
            {
                _logger.LogInformation("Thực Hiện Thêm Chuyến Đi");
                var chuyenDi = new Chuyendi
                {
                    IdChuyenDi = Guid.NewGuid(),
                    IdTrangThai = TRANG_THAI_DANG_THUC_HIEN,
                    IdTuyenDuong = input.IdTuyenDuong,
                    ThoiGianKhoiHanh = input.ThoiGianKhoiHanh,
                    ThoiGianKetThuc = input.ThoiGianKetThuc,
                    LyDoHuy = "",
                    ThoiGianHuy = null,
                    MaChuyenDi = input.MaChuyenDi,
                    GiaVe = input.GiaVe
                };
                var chuyenDi_PhuongTien = new ChuyendiThongtinphuongtien
                {
                    BienKiemSoat = input.BienKiemSoat,
                    IdChuyenDiNavigation = chuyenDi,
                    IdChuyenDi = chuyenDi.IdChuyenDi,
                    SoDienThoaiNhaXe = input.SoDienThoaiNhaXe,
                    SoGhe = input.SoGhe,
                    SoGiuong = input.SoGiuong,
                    TenNhaXe = input.TenNhaXe
                };
                foreach (var tennhanvien in input.DanhSachNhanVien)
                {
                    ChuyendiNhanvien nhanvien = new ChuyendiNhanvien()
                    {
                        IdChuyenDi = chuyenDi.IdChuyenDi,
                        HoTenNhanVien = tennhanvien,
                        IdNhanVien = Guid.NewGuid(),
                    };
                    chuyenDi.ChuyendiNhanviens.Add(nhanvien);
                }
                _context.Chuyendis.Add(chuyenDi);
                _context.ChuyendiThongtinphuongtiens.Add(chuyenDi_PhuongTien);
                _logger.LogInformation("Lưu Dữ liệu vào Database");
                _context.SaveChanges();
                _logger.LogInformation("Thêm Chuyến Đi Thành Công");
                return true;
            }catch(Exception e)
            {
                _logger.LogError(e, "Error");
                return false;
            }            
        }
        //Chuyen trang thai chuyen di
        public bool ChuyenTrangThai(Guid id)
        {
            try
            {
                _logger.LogInformation("Thực Hiện Chuyển Trạng Thái");
                var chuyendi = _context.Chuyendis.FirstOrDefault(x => x.IdChuyenDi == id);
                if (chuyendi != null && chuyendi.IdTrangThai == TRANG_THAI_DANG_THUC_HIEN)
                {
                    chuyendi.IdTrangThai = TRANG_THAI_DA_HOAN_THANH;
                    _context.SaveChanges();
                    _logger.LogInformation("Chuyển trạng thái Thành Công");
                    return true;
                }
                _logger.LogInformation("Chuyển trạng thái Thất Bại");
                return false;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error");
                return false;
            }           
        }
        public bool HuyChuyenDi(Guid id, HuyChuyenDi cd)
        {
            try
            {
                _logger.LogInformation("Thực Hiện Hủy Chuyến Đi");
                var chuyendi = _context.Chuyendis.FirstOrDefault(x => x.IdChuyenDi == id);
                if (chuyendi != null && chuyendi.IdTrangThai == TRANG_THAI_DANG_THUC_HIEN)
                {
                    chuyendi.IdTrangThai = TRANG_THAI_HUY;
                    chuyendi.LyDoHuy = cd.LyDoHuy;
                    chuyendi.ThoiGianHuy = DateTime.Now;
                    _context.SaveChanges();
                    _logger.LogInformation("Hủy Thành Công");
                    return true;
                }
                else
                {
                    _logger.LogInformation("Hủy Chuyến Thất Bại");
                    return false;
                }
            }catch(Exception e)
            {
                _logger.LogError(e, "Error");
                return false;
            }
            
        }
        //danh sach chuyen di da hoan thanh
        public List<ChuyenDiHoanThanh> DSChuyendiHoanThanh()
        {
            //query
            try
            {
                _logger.LogInformation("Liệt kê danh sách chuyến đi đã hoàn thành xuất phát từ bến xe Thái Nguyên");
                var ds = from chuyendi in _context.Chuyendis
                         join nhanvien in _context.ChuyendiNhanviens
                         on chuyendi.IdChuyenDi equals nhanvien.IdChuyenDi
                         join phuongtien in _context.ChuyendiThongtinphuongtiens
                         on chuyendi.IdChuyenDi equals phuongtien.IdChuyenDi
                         join tuyenduong in _context.Tuyenduongs on chuyendi.IdTuyenDuong equals tuyenduong.IdTuyenDuong
                         join bendi in _context.Benxes on tuyenduong.IdBenXuatPhat equals bendi.IdBenXe
                         join benden in _context.Benxes on tuyenduong.IdBenDen equals benden.IdBenXe
                         where tuyenduong.IdBenXuatPhat == ID_BEN_XE_THAI_NGUYEN && chuyendi.IdTrangThai == TRANG_THAI_DA_HOAN_THANH
                         group new { chuyendi, nhanvien, phuongtien, tuyenduong, bendi, benden }
                         by new
                         {
                             chuyendi.IdChuyenDi,
                             chuyendi.MaChuyenDi,
                             phuongtien.TenNhaXe,
                             phuongtien.BienKiemSoat,
                             phuongtien.SoDienThoaiNhaXe,
                             TenBenDi = bendi.TenBenXe,
                             TenBenDen = benden.TenBenXe,
                             chuyendi.ThoiGianKhoiHanh,
                             chuyendi.ThoiGianKetThuc
                         } into groupChuyenDi
                         select new ChuyenDiHoanThanh
                         {
                             MaChuyenDi = groupChuyenDi.Key.MaChuyenDi,
                             TenNhaXe = groupChuyenDi.Key.TenNhaXe,
                             BienKiemSoat = groupChuyenDi.Key.BienKiemSoat,
                             SoDienThoaiNhaXe = groupChuyenDi.Key.SoDienThoaiNhaXe,
                             BenDi = groupChuyenDi.Key.TenBenDi,
                             BenDen = groupChuyenDi.Key.TenBenDen,
                             HoTenNhanVien = String.Join(" - ", groupChuyenDi.Select(k => k.nhanvien.HoTenNhanVien)),
                             ThoiGianKhoiHanh = groupChuyenDi.Key.ThoiGianKhoiHanh,
                             ThoiGianKetThuc = groupChuyenDi.Key.ThoiGianKetThuc,
                         };
                if (ds.ToList() == null || ds.ToList().Count == 0)
                {
                    _logger.LogInformation("Không có chuyến đi nào");
                }
                else
                {
                    _logger.LogInformation("Liệt kê thành công");
                }
                return ds.ToList();
            }catch(Exception e)
            {
                _logger.LogError(e, "Error");
                return new List<ChuyenDiHoanThanh>();
            }                       
        }
        //danh sach chuyen di da huy
        public List<ChuyenDiHuy> DSChuyenHuy()
        {
            try
            {
                _logger.LogInformation("Liệt kê danh sách chuyến xe đã hủy");
                var danhSachChuyenHuy = from chuyenDi in _context.Chuyendis
                                        join phuongTien in _context.ChuyendiThongtinphuongtiens
                                        on chuyenDi.IdChuyenDi equals phuongTien.IdChuyenDi
                                        join tuyenDuong in _context.Tuyenduongs on chuyenDi.IdTuyenDuong equals tuyenDuong.IdTuyenDuong
                                        join benDi in _context.Benxes on tuyenDuong.IdBenXuatPhat equals benDi.IdBenXe
                                        join benDen in _context.Benxes on tuyenDuong.IdBenDen equals benDen.IdBenXe
                                        join tinhDi in _context.Tinhs on benDi.IdTinh equals tinhDi.IdTinh
                                        join tinhDen in _context.Tinhs on benDen.IdTinh equals tinhDen.IdTinh
                                        where chuyenDi.IdTrangThai == TRANG_THAI_HUY
                                        select new ChuyenDiHuy
                                        {
                                            MaChuyenDi = chuyenDi.MaChuyenDi,
                                            TenNhaXe = phuongTien.TenNhaXe,
                                            BienKiemSoat = phuongTien.BienKiemSoat,
                                            SoDienThoaiNhaXe = phuongTien.SoDienThoaiNhaXe,
                                            MaTuyen = tuyenDuong.MaTuyen,
                                            TinhDi = tinhDi.TenTinh,
                                            TinhDen = tinhDen.TenTinh,
                                            ThoiGianKhoiHanh = chuyenDi.ThoiGianKhoiHanh,
                                            LyDoHuy = chuyenDi.LyDoHuy,
                                            ThoiGianHuy = chuyenDi.ThoiGianHuy,
                                        };
                if (danhSachChuyenHuy.ToList() == null || danhSachChuyenHuy.ToList().Count == 0)
                {
                    _logger.LogInformation("Không có chuyến đi nào");
                }
                else
                {
                    _logger.LogInformation("Liệt kê thành công");
                }
                return danhSachChuyenHuy.ToList();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error");
                return new List<ChuyenDiHuy>();
            }            
        }
    }
}
