﻿namespace ChuyenDiFinal.Models.ViewModel
{
    public class KhachHang
    {
        public string HoTen { get; set; } = null!;
        public string SoDienThoai { get; set; } = null!;
        public string Email { get; set; } = null!;
    }
}
