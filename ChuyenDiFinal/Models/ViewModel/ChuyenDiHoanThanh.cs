﻿namespace ChuyenDiFinal.Models.ViewModel
{
    public class ChuyenDiHoanThanh
    {
        /// Mã chuyến đi
        public string MaChuyenDi { get; set; } = null!;
        /// Tên nhà xe
        public string TenNhaXe { get; set; } = null!;
        /// Biển kiểm soát
        public string BienKiemSoat { get; set; } = null!;
        /// Số điện thoại nhà xe
        public string SoDienThoaiNhaXe { get; set; } = null!;
        /// Ten Ben Di
        public string BenDi { get; set; } = null;
        /// Ten Ben Den
        public string BenDen { get; set; } = null;
        /// Họ tên nhân viên
        public string HoTenNhanVien { get; set; }
        /// Thời gian khởi hành
        public DateTime ThoiGianKhoiHanh { get; set; }
        /// Thời gian kết thúc
        public DateTime ThoiGianKetThuc { get; set; }
    }
}
