﻿namespace ChuyenDiFinal.Models.ViewModel
{
    public class MuaVeXe
    {
        public Guid IdChuyenDi { get; set; }
        public string SoDienThoai { get; set; } = null!;
        public string HoTen { get; set; } = null!;
        public string Email { get; set; } = null!;
    }
}
