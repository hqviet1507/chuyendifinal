﻿namespace ChuyenDiFinal.Models.ViewModel
{
    public class ThemChuyenDiInput
    {
        public Guid IdTuyenDuong { get; set; }
        public string MaChuyenDi { get; set; } = null!;
        public List<string> DanhSachNhanVien { get; set; }
        public DateTime ThoiGianKhoiHanh { get; set; }
        public DateTime ThoiGianKetThuc { get; set; }
        public string TenNhaXe { get; set; } = null!;
        public string SoDienThoaiNhaXe { get; set; } = null!;
        public string BienKiemSoat { get; set; } = null!;
        public int SoGhe { get; set; }
        public int SoGiuong { get; set; }
        public decimal? GiaVe { get; set; }
    }
}
