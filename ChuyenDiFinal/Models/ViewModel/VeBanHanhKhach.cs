﻿namespace ChuyenDiFinal.Models.ViewModel
{
    public class VeBanHanhKhach
    {
        public string SoDienThoai { get; set; }
        public string HoTen { get; set; }
        public string Email { get; set; }
        public List<DanhSachChuyenDiDaMua> DanhSachDaMua { get; set; }
    }
}
