﻿using ChuyenDiFinal.Models.Database;

namespace ChuyenDiFinal.Models.ViewModel
{
    public class ThongKeHanhKhach
    {
        public string MaChuyenDi { get; set; }
        public string BenDi { get; set; }
        public string BenDen { get; set; }
        public DateTime ThoiGianKhoiHanh { get; set; }
        public DateTime ThoiGianKetThuc { get; set; }
        public string BienKiemSoat { get; set; } = null!;
        public List<KhachHang> ThongTinKhachHang { get; set; }
    }
}
