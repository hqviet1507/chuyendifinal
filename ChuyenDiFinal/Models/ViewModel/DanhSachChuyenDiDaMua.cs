﻿namespace ChuyenDiFinal.Models.ViewModel
{
    public class DanhSachChuyenDiDaMua
    {
        public string MaChuyenDi { get; set; }
        public string BienKiemSoat { get; set; }
        public string BenDi { get; set; }
        public string BenDen { get; set; }
    }
}
