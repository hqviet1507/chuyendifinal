﻿namespace ChuyenDiFinal.Models.ViewModel
{
    public class ThongKeVeXe
    {
        public string MaChuyenDi { get; set; } = null!;
        public string BenDi { get; set; } = null;
        public string BenDen { get; set; } = null;
        public DateTime ThoiGianKhoiHanh { get; set; }
        public DateTime ThoiGianKetThuc { get; set; }
        public string BienKiemSoat { get; set; } = null!;
        public decimal? SoLuongVeBan { get; set; }
        public decimal? TongTien { get; set; }
    }
}
