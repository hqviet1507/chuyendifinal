﻿namespace ChuyenDiFinal.Models.ViewModel
{
    public class ChuyenDiHuy
    {
        public string MaChuyenDi { get; set; } = null!;
        public string TenNhaXe { get; set; } = null!;
        public string BienKiemSoat { get; set; } = null!;
        public string SoDienThoaiNhaXe { get; set; } = null!;
        public string MaTuyen { get; set; } = null!;
        public string TinhDi { get; set; } = null;
        public string TinhDen { get; set; } = null;
        public DateTime ThoiGianKhoiHanh { get; set; }
        public string? LyDoHuy { get; set; }
        public DateTime? ThoiGianHuy { get; set; }
    }
}
