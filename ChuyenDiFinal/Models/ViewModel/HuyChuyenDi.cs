﻿using System.ComponentModel.DataAnnotations;

namespace ChuyenDiFinal.Models.ViewModel
{
    public class HuyChuyenDi
    {
        [Required(ErrorMessage = "ly do huy khong duoc de trong")]
        [MinLength(1)]
        public string LyDoHuy { get; set; }
    }
}
