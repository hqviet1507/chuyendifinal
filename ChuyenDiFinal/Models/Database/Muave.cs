﻿using System;
using System.Collections.Generic;

namespace ChuyenDiFinal.Models.Database
{
    public partial class Muave
    {
        public Muave()
        {
            Doisoats = new HashSet<Doisoat>();
        }

        public Guid IdMuaVe { get; set; }
        public Guid IdChuyenDi { get; set; }
        public Guid IdHanhKhach { get; set; }
        public DateTime ThoiGianMuaVe { get; set; }

        public virtual Chuyendi IdChuyenDiNavigation { get; set; } = null!;
        public virtual Hanhkhach IdHanhKhachNavigation { get; set; } = null!;
        public virtual ICollection<Doisoat> Doisoats { get; set; }
    }
}
