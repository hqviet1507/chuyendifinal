﻿using System;
using System.Collections.Generic;

namespace ChuyenDiFinal.Models.Database
{
    public partial class Trangthai
    {
        public Trangthai()
        {
            Chuyendis = new HashSet<Chuyendi>();
        }

        public Guid IdTrangThai { get; set; }
        public string TenTrangThai { get; set; } = null!;

        public virtual ICollection<Chuyendi> Chuyendis { get; set; }
    }
}
