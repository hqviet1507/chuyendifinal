﻿using System;
using System.Collections.Generic;

namespace ChuyenDiFinal.Models.Database
{
    public partial class Hanhkhach
    {
        public Hanhkhach()
        {
            Muaves = new HashSet<Muave>();
        }

        public Guid IdHanhKhach { get; set; }
        public string HoTen { get; set; } = null!;
        public string SoDienThoai { get; set; } = null!;
        public string Email { get; set; } = null!;

        public virtual ICollection<Muave> Muaves { get; set; }
    }
}
