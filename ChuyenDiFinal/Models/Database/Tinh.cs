﻿using System;
using System.Collections.Generic;

namespace ChuyenDiFinal.Models.Database
{
    public partial class Tinh
    {
        public Tinh()
        {
            Benxes = new HashSet<Benxe>();
        }

        public Guid IdTinh { get; set; }
        public string TenTinh { get; set; } = null!;

        public virtual ICollection<Benxe> Benxes { get; set; }
    }
}
