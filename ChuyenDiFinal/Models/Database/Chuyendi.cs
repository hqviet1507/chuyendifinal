﻿using System;
using System.Collections.Generic;

namespace ChuyenDiFinal.Models.Database
{
    public partial class Chuyendi
    {
        public Chuyendi()
        {
            ChuyendiNhanviens = new HashSet<ChuyendiNhanvien>();
            Muaves = new HashSet<Muave>();
        }

        public Guid IdChuyenDi { get; set; }
        public Guid IdTuyenDuong { get; set; }
        public Guid IdTrangThai { get; set; }
        public string MaChuyenDi { get; set; } = null!;
        public DateTime ThoiGianKhoiHanh { get; set; }
        public DateTime ThoiGianKetThuc { get; set; }
        public string? LyDoHuy { get; set; }
        public DateTime? ThoiGianHuy { get; set; }
        public decimal? GiaVe { get; set; }

        public virtual Trangthai IdTrangThaiNavigation { get; set; } = null!;
        public virtual Tuyenduong IdTuyenDuongNavigation { get; set; } = null!;
        public virtual ChuyendiThongtinphuongtien? ChuyendiThongtinphuongtien { get; set; }
        public virtual ICollection<ChuyendiNhanvien> ChuyendiNhanviens { get; set; }
        public virtual ICollection<Muave> Muaves { get; set; }
    }
}
