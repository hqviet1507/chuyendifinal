﻿using System;
using System.Collections.Generic;

namespace ChuyenDiFinal.Models.Database
{
    public partial class Tuyenduong
    {
        public Tuyenduong()
        {
            Chuyendis = new HashSet<Chuyendi>();
        }

        public Guid IdTuyenDuong { get; set; }
        public Guid IdBenXuatPhat { get; set; }
        public Guid IdBenDen { get; set; }
        public string MaTuyen { get; set; } = null!;
        public int CuLy { get; set; }

        public virtual Benxe IdBenDenNavigation { get; set; } = null!;
        public virtual Benxe IdBenXuatPhatNavigation { get; set; } = null!;
        public virtual ICollection<Chuyendi> Chuyendis { get; set; }
    }
}
