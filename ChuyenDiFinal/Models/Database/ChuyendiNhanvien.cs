﻿using System;
using System.Collections.Generic;

namespace ChuyenDiFinal.Models.Database
{
    public partial class ChuyendiNhanvien
    {
        public Guid IdNhanVien { get; set; }
        public Guid IdChuyenDi { get; set; }
        public string HoTenNhanVien { get; set; } = null!;

        public virtual Chuyendi IdChuyenDiNavigation { get; set; } = null!;
    }
}
