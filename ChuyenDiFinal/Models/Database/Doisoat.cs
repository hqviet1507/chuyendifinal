﻿using System;
using System.Collections.Generic;

namespace ChuyenDiFinal.Models.Database
{
    public partial class Doisoat
    {
        public Guid IdDoiSoat { get; set; }
        public Guid IdMuaVe { get; set; }
        public DateTime ThoiGianDoiSoat { get; set; }

        public virtual Muave IdMuaVeNavigation { get; set; } = null!;
    }
}
