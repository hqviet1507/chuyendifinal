using ChuyenDiFinal.Models.Database;
using ChuyenDiFinal.Services;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Serilog.Sinks.Graylog;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<ChuyenDiContext>(
    o => o.UseNpgsql(builder.Configuration.GetConnectionString("Npgsql")));
builder.Services.AddScoped<ChuyenDiService>();
builder.Services.AddScoped<VeXeService>();

builder.Logging.ClearProviders();

var logger = new LoggerConfiguration()
            .ReadFrom.Configuration(builder.Configuration, sectionName: "Serilog")
            .WriteTo.Graylog("10.10.0.155", 15070, Serilog.Sinks.Graylog.Core.Transport.TransportType.Http)
            .WriteTo.Console()
            .CreateLogger();
builder.Logging.AddSerilog(logger);
builder.WebHost.UseSerilog(logger);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
